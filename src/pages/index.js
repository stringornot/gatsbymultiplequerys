import React from "react"
import { StaticQuery } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"

const IndexPage = ({data}) => (
  <Layout>
    <SEO title="Home" />
    <StaticQuery
      query={graphql`
         query {
          placeholderImage: file(relativePath: { eq: "gatsby-astronaut.png" }) {
            childImageSharp {
              fluid(maxWidth: 300) {
                ...GatsbyImageSharpFluid
              }
            }
          }
        }
      `}
      render={staticData => (
        <>
          <h1>StaticQuery:</h1>
          <pre>
            {JSON.stringify(staticData, null, 4)}
          </pre>
          <h1>Page query data:</h1>
          <pre>
            {JSON.stringify(data, null, 4)}
          </pre>
        </>
      )}
    />

    <h1>Page query data outside StaticQuery:</h1>
    <pre>
      {JSON.stringify(data, null, 4)}
    </pre>

  </Layout>
)

export default IndexPage

export const query = graphql`
    {
        allGhostPost {
            edges {
                node {
                    id
                    feature_image
                    title
                    url
                }
            }
        }
    }
`